git-autofix
===========
What if there was an extension to git that made everything better. What if it automatically fixed everything or at least told you what you needed to do next. It could educate the git newbies - or remind the beaten down veterans - with informative information, links and funny quips. This could be that script. 

For now, though, it's just a young sarcastic, relatively unhelpful and unoriginal script that points you right back to the manual. Will it ever grow up?

Installation
===========
From my basic understanding, after two minutes on Stack Overflow, git will automatically alias a command like `git autofix` to `git-autofix`. So, all you have to do is have the `git-autofix` script in your path somewhere and it will automatically pick it up. Simple enough, right? 

Personally, on my mac, i have a `~/.bash_profile` file and just added `export PATH=$PATH:~/bin
`. Then all I have to do is drop in custom scripts into the `~/bin` directory and I'm good.

`curl -o git-autofix https://raw.github.com/jakeasmith/git-autofix/master/git-autofix; chmod 755 git-autofix;`